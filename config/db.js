const { connect } = require('mongoose');

const connectDB = async () => {
    try {
        const conn = await connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
        console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline.bold);        
    } catch (error) {
        console.log(`MongoDB Not Connected: ${conn.connection.host}`.red.underline.bold);                
    }
};

module.exports = connectDB;