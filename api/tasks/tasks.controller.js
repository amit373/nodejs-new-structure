const taskModel = require('./tasks.model');
const asyncHandler = require('../../middleware/asyncHandler');
const APIFeatures = require('./../../utils/apiFeatures');
const AppError = require('./../../utils/appError');
const { validateTaskSchema } = require('./tasks.service');

// @desc      Create Tasks
// @route     POST /api/v1/tasks/
// @access    Public
exports.createTask = asyncHandler(async (req, res, next) => {
    const { value, error } = validateTaskSchema(req.body);
    if (error && error.details) return next(new AppError(error.details[0].message, 500));
    
    const task = await taskModel.create({ title: value.title, description: value.description, status: value.status });
    res.status(201).json({ status: true, data: task, message: 'task created' });
});

// @desc      Get All Tasks
// @route     GET /api/v1/tasks/
// @access    Public
exports.getAllTask = asyncHandler(async (req, res, next) => {
    const features = new APIFeatures(taskModel.find(), req.query).sort().paginate().limitFields();
    const tasks = await features.query;
    res.status(200).json({ status: true, data: tasks, message:'get all tasks' });
});

// @desc      Get Task
// @route     GET /api/v1/tasks/id
// @access    Private
exports.getTaskById = asyncHandler(async (req, res, next) => {
    const task = await taskModel.findById(req.params.id);
    res.status(200).json({ status: true, data: task, message: 'task by id' });
});

// @desc      Update Task
// @route     POST /api/v1/tasks/id
// @access    Private
exports.updateTask = asyncHandler(async (req, res, next) => {
    task = await taskModel.findByIdAndUpdate(req.params.id, req.body, {
       new: true, runValidators: true
    });
    res.status(200).json({ status: true, data: task, message: `task updated with id:${req.params.id}` });
});


// @desc      Delete Task
// @route     POST /api/v1/tasks/id
// @access    Private
exports.deleteTask = asyncHandler(async (req, res, next) => {
    await taskModel.findByIdAndDelete(req.params.id);
    res.status(200).json({ status: true, data: null, message: `task deleted with id:${req.params.id}`});
});
