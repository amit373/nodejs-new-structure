const { Schema, model } = require('mongoose');

const taskSchema = new Schema({
    title: {
      type: String,
      required: [true, 'title is Required'],
      unique: true,
      trim: true,
      maxlength: [100, 'title can not be more than 100 characters']
    },
    description: {
      type: String,
      required: [true, 'description is Required'],
      maxlength: [500, 'description can not be more than 100 characters']
    },
    dueDate: {
      type: Date,
      default: Date.now    
    },
    status: {
      type: String,
      enum:['OPEN', 'CLOSE'],
      default:'OPEN',
      required: [true, 'status is Required'],
    },
    createdAt: {
      type: Date,
      default: Date.now
    }
  });

module.exports = model('task',taskSchema);