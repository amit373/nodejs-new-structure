const express = require('express');
const router = express.Router();

const { verifyToken } = require('./../../middleware/verifyToken');
const { createTask, getAllTask, getTaskById, updateTask, deleteTask } = require('./tasks.controller');

router.route('/tasks')
    .get(verifyToken, getAllTask)
    .post(verifyToken, createTask);
router.route('/tasks/:id')
    .get(verifyToken, getTaskById)
    .put(verifyToken, updateTask)
    .delete(verifyToken, deleteTask);

module.exports = router;