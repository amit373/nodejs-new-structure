const Joi = require('joi');

exports.validateTaskSchema = (body) => {
  const schema = Joi.object().keys({
    title: Joi.string().required().min(0).max(100),
    description: Joi.string().required().min(0).max(500),
    dueDate: Joi.optional(),
    status: Joi.string().required(),
  });
  const { error, value } = Joi.validate(body, schema);
  if (error && error.details) {
    return { error };
  }
  return { value };
};
