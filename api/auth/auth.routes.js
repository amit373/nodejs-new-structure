const express = require('express');
const router = express.Router();

const { register, login, logout, getMe, updateDetails, updatePassword, forgotPassword, resetPassword } = require('./auth.controller');
const { verifyToken } = require('./../../middleware/verifyToken');

router.post('/register', register);
router.post('/login', login);
router.get('/logout', verifyToken, logout);
router.get('/me', verifyToken, getMe);
router.put('/updatedetails', verifyToken, updateDetails);
router.put('/updatepassword', verifyToken, updatePassword);
router.post('/forgotpassword', forgotPassword);
router.put('/resetpassword/:resettoken', resetPassword);

module.exports = router;